import {Schema} from 'mongoose';
import {RawHotelSchema} from './rawHotel.schema';
import {RawSearchResultDocument} from '..';

export const RawSearchResultSchemaKey: string = 'rawSearchResult';

export const RawSearchResultSchema = (schema: typeof Schema): Schema<RawSearchResultDocument> => new schema({
        searchId: String,
        searchPerformedForPlace: String,
        scrapingTimeSeconds: Number,
        searchProcessTimeSeconds: Number,
        hotelsCount: Number,
        hotels: [RawHotelSchema(schema)],
    },
    {
        versionKey: false,
        timestamps: {
            createdAt: true,
            updatedAt: false,
        },
    });
