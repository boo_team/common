import {Schema} from 'mongoose';
import {RawRoomDocument} from '..';

export const RawRoomSchema = (schema: typeof Schema): Schema<RawRoomDocument> => new schema({
        description: String,
        personCount: String,
        beds: String,
    },
    {
        _id: false,
        versionKey: false,
    });
