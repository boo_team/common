import {Schema} from 'mongoose';
import {RawRoomSchema} from './rawRoomSchema';
import {RawHotelDocument} from '..';

export const RawHotelSchema = (schema: typeof Schema): Schema<RawHotelDocument> => new schema({
        hotelId: String,
        // following parameters always exist
        name: String,
        price: String,
        tax: String,
        distanceFromCenter: String,
        districtName: String,
        coords: String,
        hotelLink: String,
        // following parameters might not be available
        rate: String,
        secondaryRateType: String,
        secondaryRate: String,
        priceWithoutDiscount: String,
        numberOfReviews: String,
        propertyType: String,
        starRating: String,
        newlyAdded: String,
        bonuses: [String],
        rooms: [RawRoomSchema(schema)],
    },
    {
        _id: false,
        versionKey: false,
    });
