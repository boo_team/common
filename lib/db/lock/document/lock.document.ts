import {Document} from 'mongoose';
import {TimestampsDocument} from '../../interface';

export interface LockDocument extends Document, Pick<TimestampsDocument, 'createdAt'> {
}
