import {Schema} from 'mongoose';
import {LockDocument} from '..';

export const LockSchemaKey: string = 'lock';

export const LockSchema = (schema: typeof Schema): Schema<LockDocument> => new schema({},
    {
        versionKey: false,
        timestamps: {
            createdAt: true,
        },
    });
