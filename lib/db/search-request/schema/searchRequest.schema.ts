import {Schema} from 'mongoose';
import {CheckDateSchema} from './checkDate.schema';
import {ChildrenPropertiesSchema} from './childrenProperties.schema';
import {SearchRequestDocument} from '..';

export const SearchRequestSchemaKey: string = 'searchRequest';

export const SearchRequestSchema = (schema: typeof Schema): Schema<SearchRequestDocument> => new schema({
        searchId: String,
        priority: Number,
        updateFrequencyMinutes: Number,
        resultsLimit: Number,
        occupancyStatus: String,
        occupancyUpdatedAt: Date,
        owner: String,
        // Scenario parameters
        city: String,
        checkInDate: CheckDateSchema(schema),
        checkOutDate: CheckDateSchema(schema),
        numberOfRooms: Number,
        numberOfAdults: Number,
        numberOfChildren: [ChildrenPropertiesSchema(schema)],
        currency: String,
        language: String,
    },
    {
        versionKey: false,
        timestamps: {
            createdAt: true,
            updatedAt: true,
        },
    });
