import {Schema} from 'mongoose';
import {CheckDateDocument} from '..';

export const CheckDateSchema = (schema: typeof Schema): Schema<CheckDateDocument> => new schema({
        year: Number,
        month: Number,
        day: Number,
    },
    {
        _id: false,
        versionKey: false,
    });
