import {Schema} from 'mongoose';
import {ChildrenPropertiesDocument} from '..';

export const ChildrenPropertiesSchema = (schema: typeof Schema): Schema<ChildrenPropertiesDocument> => new schema({
        yearAgeAtCheckOut: Number,
    },
    {
        _id: false,
        versionKey: false,
    });
