import {Document} from 'mongoose';
import {TimestampsDocument} from '../../interface';
import {ChildrenPropertiesDocument} from './childrenProperties.document';
import {CheckDateDocument} from './checkDate.document';
import {ObjectId} from 'bson';

export interface SearchRequestDocument extends Document, TimestampsDocument {
    readonly _id: ObjectId;
    readonly searchId: string;
    readonly priority: number;
    readonly updateFrequencyMinutes: number;
    readonly resultsLimit: number;
    readonly occupancyStatus: string;
    readonly occupancyUpdatedAt: string;
    // Scenario parameters
    readonly city: string;
    readonly checkInDate: CheckDateDocument;
    readonly checkOutDate: CheckDateDocument;
    readonly numberOfRooms: number;
    readonly numberOfAdults: number;
    readonly numberOfChildren: ChildrenPropertiesDocument[];
    readonly currency: string;
    readonly language: string;
}
