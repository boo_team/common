import {Document} from 'mongoose';

export interface ChildrenPropertiesDocument extends Document {
    readonly yearAgeAtCheckOut: number;
}
