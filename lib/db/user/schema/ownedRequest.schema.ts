import {Schema} from 'mongoose';

export const OwnedRequestSchema = (schema: typeof Schema): Schema => new schema({
        searchId: String,
        favorites: [String],
    },
    {
        _id: false,
        versionKey: false,
    });
