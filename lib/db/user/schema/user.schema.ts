import {Schema} from 'mongoose';
import {OwnedRequestSchema} from './ownedRequest.schema';
import {UserDocument} from '..';

export const UserSchemaKey: string = 'user';

export const UserSchema = (schema: typeof Schema): Schema<UserDocument> => new schema({
        email: String,
        password: String,
        ownedSearchRequests: [OwnedRequestSchema(schema)],
        roles: [String],
        requestsLimit: Number,
    },
    {
        versionKey: false,
        timestamps: {
            createdAt: true,
            updatedAt: true,
        },
    });
