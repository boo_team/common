export interface OwnedRequestDocument extends Document {
    readonly searchId: string;
    readonly favorites: string[];
}
