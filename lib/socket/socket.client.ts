import {fromEvent, merge, Observable, ReplaySubject} from 'rxjs';
import {mapTo, switchMap, tap} from 'rxjs/operators';
import {Logger} from '..';
import io = require('socket.io-client');
import Socket = SocketIOClient.Socket;

export class SocketClient {

    private client: Socket;
    private socketSubject: ReplaySubject<Socket>;

    constructor(private readonly logger: Logger) {
        this.socketSubject = new ReplaySubject(1);
    }

    public connect(socketServerAddress: string): void {
        if (!!this.client) {
            this.client.connect();
        } else {
            this.initClientAndConnect(socketServerAddress).subscribe();
        }
    }

    /**
     * After disconnecting it is not needed to initialize client once again when you want do connect again
     */
    public disconnect(): void {
        if (this.isConnected()) {
            this.client.disconnect();
        } else {
            this.logger.warn('Already disconnected from socket');
        }
    }

    public watchEvent<T>(event: string): Observable<T> {
        return this.socketSubject.pipe(
            switchMap((socket: Socket) => fromEvent<T>(socket, event)),
        );
    }

    public sendIfConnected<T>(event: string, message: T): void {
        if (this.isConnected()) {
            this.client.emit(event, message);
            this.logger.debug(`[${event}] event send. Message: `, message);
        } else {
            this.logger.warn(`[${event}] event cannot be sent. Socket server is disconnected.`);
        }
    }

    private initClientAndConnect(socketServerAddress: string): Observable<boolean> {
        this.logger.debug(`Waiting for socket client initialization and connect to [${socketServerAddress}]`);
        this.client = io(`${socketServerAddress}`);
        this.socketSubject.next(this.client);
        const connect$ = this.socketSubject.pipe(
            switchMap<Socket, Observable<void>>((socket: Socket) => fromEvent(socket, 'connect')),
            tap(() => this.logger.debug(`Connected to socket server [${socketServerAddress}]`)),
            mapTo(true),
        );
        const disconnect$ = this.socketSubject.pipe(
            switchMap<Socket, Observable<void>>((socket: Socket) => fromEvent(socket, 'disconnect')),
            tap(() => this.logger.debug(`Disconnected from socket server [${socketServerAddress}]`)),
            mapTo(false),
        );
        return merge(
            connect$,
            disconnect$,
        ).pipe(tap(() => {
            const connectedSockets = this.client.io.connecting || [];
            const ids = connectedSockets.filter((s) => s.connected)
                .filter((s) => s.id)
                .map((s) => s.id);
            if (ids.length) {
                this.logger.debug(`Connected socket clients ids: `, ids);
            }
        }));
    }

    public isConnected = (): boolean => !!this.client && this.client.connected;
}
