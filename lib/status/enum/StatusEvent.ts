export enum StatusEvent {
    SCRAPING_STATUS = 'SCRAPING_STATUS',
    PROCESSING_RESULT_STATUS = 'PROCESSING_RESULT_STATUS',
    SEARCH_REQUEST_UPDATE = 'SEARCH_REQUEST_UPDATE',
    NEW_SEARCH_REQUEST = 'NEW_SEARCH_REQUEST',
    SEARCH_REQUEST_DELETED = 'SEARCH_REQUEST_DELETED',
}
