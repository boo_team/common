export class AsyncHelper {
    public static async forEach<T>(collection: Array<T> | Set<T>, processPromise: (element: T) => void): Promise<void> {
        for (const element of collection) {
            await processPromise(element);
        }
    }
}