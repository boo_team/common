import {BuilderType} from './BuilderType';

export type ToBuilder<T> = T & { toBuilder: () => BuilderType<T> };
