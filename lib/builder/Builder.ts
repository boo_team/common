/**
 * The Builder decorator change class to builder.
 * When class is in builder mode (toBuilder() method invoked) than all properties are changed to setters.
 * After invoking build() method class returns to previous state. All setters are changed to new properties.
 */
import {getBuilderClass} from './getBuilderClass';

export function Builder<T extends new(...args: any[]) => {}>(constructor: T) {
    return getBuilderClass(constructor, assignSetterToKey);
}

const assignSetterToKey = (key: string, that: any) => {
    that[key] = (prop: any) => {
        that.newInstance[key] = prop;
        return that;
    };
};
