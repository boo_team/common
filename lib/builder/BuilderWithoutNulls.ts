import {getBuilderClass} from './getBuilderClass';

/**
 * The BuilderWithoutNulls decorator works the same way as Builder but when any of setters set null to property than that property is deleted.
 * This is needed when document is saved to the mongodb. Nulls will not be saved to database.
 */
export function BuilderWithoutNulls<T extends new(...args: any[]) => {}>(constructor: T) {
    return getBuilderClass(constructor, assignSetterToKeyIfNullDeleteProp);
}

const assignSetterToKeyIfNullDeleteProp = (key: string, that: any) => {
    that[key] = (prop: any) => {
        if (prop !== null) {
            that.newInstance[key] = prop;
        } else {
            delete that.newInstance[key];
        }
        return that;
    };
};
