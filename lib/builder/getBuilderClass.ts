import {BuilderType} from './interface/BuilderType';

export const getBuilderClass = <T extends new(...args: any[]) => {}>(constructor: T,
                                                                     assignSetter: (key: string, that: any) => void) => {
    return class extends constructor {

        public newInstance: T;

        constructor(...args: any[]) {
            super(...args);
        }

        public toBuilder(): BuilderType<T> {
            const oldInstance = {} as T;
            const properties = Object.getOwnPropertyNames(this);
            properties.forEach((key: string) => {
                oldInstance[key] = this[key];
                assignSetter(key, this);
            });
            this.newInstance = oldInstance;
            return this as any;
        }

        public build(): T {
            return this.newInstance;
        }
    };
};
