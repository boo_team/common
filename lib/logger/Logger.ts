import * as Winston from 'winston';

export class Logger {

    private static instance: Logger = null;
    private readonly winstonLogger: Winston.Logger;
    private customPrint = (serviceLabel: string) => this.winston.format.printf(info =>
        `[${serviceLabel}][${info.level}][${new Date().toISOString()}]: ${info.message}`)

    private constructor(private readonly winston: typeof Winston,
                        private readonly serviceLabel: string,
                        private readonly additionalTransports:
                            Array<Winston.transports.FileTransportInstance | Winston.transports.HttpTransportInstance> = [],
                        private readonly level: string = 'debug',
                        private readonly prettierJson = false,
                        private readonly color = false) {

        const formats: any[] = [];
        if (this.color) {
            formats.push(winston.format.colorize()); // Needs to be first
        }
        formats.push(this.customPrint(serviceLabel));

        this.winstonLogger = winston.createLogger({
            level,
            format: winston.format.combine(...formats),
            transports: [
                new winston.transports.Console(),
                ...additionalTransports,
            ],
        });
    }

    public debug(message: string, object?: any): void {
        if (object) {
            this.winstonLogger.debug(this.concatObjWithMessage(message, object));
        } else {
            this.winstonLogger.debug(message);
        }
    }

    // For Nest logging purpose only
    public log(message: string, context?: string): void {
        if (context) {
            this.winstonLogger.debug(this.concatObjWithMessage(message, context));
        } else {
            this.winstonLogger.debug(message);
        }
    }

    public info(message: string, object?: any): void {
        if (object) {
            this.winstonLogger.info(this.concatObjWithMessage(message, object));
        } else {
            this.winstonLogger.info(message);
        }
    }

    public warn(message: string, object?: any): void {
        if (object) {
            this.winstonLogger.warn(this.concatObjWithMessage(message, object));
        } else {
            this.winstonLogger.warn(message);
        }
    }

    // Last param is for Nest logging purpose only
    public error(message: string, error?: Error | string, context?: string): void {
        if (error) {
            this.winstonLogger.error(this.concatStackWithMessage(message, error as Error));
        } else {
            this.winstonLogger.error(message);
        }
    }

    private concatStackWithMessage(message: string, error: Error): string {
        if (error.stack) {
            const concatStack = error.stack
                .split('\n')
                .map((m) => m.trim());
            return this.concatObjWithMessage(message, concatStack);
        }
        return this.concatObjWithMessage(message, error);
    }

    private concatObjWithMessage = (message: string, object: any): string =>
        this.prettierJson
            ? `${message}${JSON.stringify(object, null, 4)}`
            : `${message}${JSON.stringify(object)}`

    public static get(winston: typeof Winston,
                      serviceName: string,
                      additionalTransports?: Array<Winston.transports.FileTransportInstance | Winston.transports.HttpTransportInstance>,
                      level?: string,
                      prettierJson?: boolean,
                      color?: boolean): Logger {
        if (!Logger.instance) {
            Logger.instance = new Logger(winston, serviceName, additionalTransports, level, prettierJson, color);
        }
        return Logger.instance;
    }
}