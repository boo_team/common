import * as Winston from 'winston';
export declare class Logger {
    private readonly winston;
    private readonly serviceLabel;
    private readonly additionalTransports;
    private readonly level;
    private readonly prettierJson;
    private readonly color;
    private static instance;
    private readonly winstonLogger;
    private customPrint;
    private constructor();
    debug(message: string, object?: any): void;
    log(message: string, context?: string): void;
    info(message: string, object?: any): void;
    warn(message: string, object?: any): void;
    error(message: string, error?: Error | string, context?: string): void;
    private concatStackWithMessage;
    private concatObjWithMessage;
    static get(winston: typeof Winston, serviceName: string, additionalTransports?: Array<Winston.transports.FileTransportInstance | Winston.transports.HttpTransportInstance>, level?: string, prettierJson?: boolean, color?: boolean): Logger;
}
