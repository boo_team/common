"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Logger {
    constructor(winston, serviceLabel, additionalTransports = [], level = 'debug', prettierJson = false, color = false) {
        this.winston = winston;
        this.serviceLabel = serviceLabel;
        this.additionalTransports = additionalTransports;
        this.level = level;
        this.prettierJson = prettierJson;
        this.color = color;
        this.customPrint = (serviceLabel) => this.winston.format.printf(info => `[${serviceLabel}][${info.level}][${new Date().toISOString()}]: ${info.message}`);
        this.concatObjWithMessage = (message, object) => this.prettierJson
            ? `${message}${JSON.stringify(object, null, 4)}`
            : `${message}${JSON.stringify(object)}`;
        const formats = [];
        if (this.color) {
            formats.push(winston.format.colorize()); // Needs to be first
        }
        formats.push(this.customPrint(serviceLabel));
        this.winstonLogger = winston.createLogger({
            level,
            format: winston.format.combine(...formats),
            transports: [
                new winston.transports.Console(),
                ...additionalTransports,
            ],
        });
    }
    debug(message, object) {
        if (object) {
            this.winstonLogger.debug(this.concatObjWithMessage(message, object));
        }
        else {
            this.winstonLogger.debug(message);
        }
    }
    // For Nest logging purpose only
    log(message, context) {
        if (context) {
            this.winstonLogger.debug(this.concatObjWithMessage(message, context));
        }
        else {
            this.winstonLogger.debug(message);
        }
    }
    info(message, object) {
        if (object) {
            this.winstonLogger.info(this.concatObjWithMessage(message, object));
        }
        else {
            this.winstonLogger.info(message);
        }
    }
    warn(message, object) {
        if (object) {
            this.winstonLogger.warn(this.concatObjWithMessage(message, object));
        }
        else {
            this.winstonLogger.warn(message);
        }
    }
    // Last param is for Nest logging purpose only
    error(message, error, context) {
        if (error) {
            this.winstonLogger.error(this.concatStackWithMessage(message, error));
        }
        else {
            this.winstonLogger.error(message);
        }
    }
    concatStackWithMessage(message, error) {
        if (error.stack) {
            const concatStack = error.stack
                .split('\n')
                .map((m) => m.trim());
            return this.concatObjWithMessage(message, concatStack);
        }
        return this.concatObjWithMessage(message, error);
    }
    static get(winston, serviceName, additionalTransports, level, prettierJson, color) {
        if (!Logger.instance) {
            Logger.instance = new Logger(winston, serviceName, additionalTransports, level, prettierJson, color);
        }
        return Logger.instance;
    }
}
Logger.instance = null;
exports.Logger = Logger;
