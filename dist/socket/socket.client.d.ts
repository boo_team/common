import { Observable } from 'rxjs';
import { Logger } from '..';
export declare class SocketClient {
    private readonly logger;
    private client;
    private socketSubject;
    constructor(logger: Logger);
    connect(socketServerAddress: string): void;
    /**
     * After disconnecting it is not needed to initialize client once again when you want do connect again
     */
    disconnect(): void;
    watchEvent<T>(event: string): Observable<T>;
    sendIfConnected<T>(event: string, message: T): void;
    private initClientAndConnect;
    isConnected: () => boolean;
}
