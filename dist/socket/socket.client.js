"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const rxjs_1 = require("rxjs");
const operators_1 = require("rxjs/operators");
const io = require("socket.io-client");
class SocketClient {
    constructor(logger) {
        this.logger = logger;
        this.isConnected = () => !!this.client && this.client.connected;
        this.socketSubject = new rxjs_1.ReplaySubject(1);
    }
    connect(socketServerAddress) {
        if (!!this.client) {
            this.client.connect();
        }
        else {
            this.initClientAndConnect(socketServerAddress).subscribe();
        }
    }
    /**
     * After disconnecting it is not needed to initialize client once again when you want do connect again
     */
    disconnect() {
        if (this.isConnected()) {
            this.client.disconnect();
        }
        else {
            this.logger.warn('Already disconnected from socket');
        }
    }
    watchEvent(event) {
        return this.socketSubject.pipe(operators_1.switchMap((socket) => rxjs_1.fromEvent(socket, event)));
    }
    sendIfConnected(event, message) {
        if (this.isConnected()) {
            this.client.emit(event, message);
            this.logger.debug(`[${event}] event send. Message: `, message);
        }
        else {
            this.logger.warn(`[${event}] event cannot be sent. Socket server is disconnected.`);
        }
    }
    initClientAndConnect(socketServerAddress) {
        this.logger.debug(`Waiting for socket client initialization and connect to [${socketServerAddress}]`);
        this.client = io(`${socketServerAddress}`);
        this.socketSubject.next(this.client);
        const connect$ = this.socketSubject.pipe(operators_1.switchMap((socket) => rxjs_1.fromEvent(socket, 'connect')), operators_1.tap(() => this.logger.debug(`Connected to socket server [${socketServerAddress}]`)), operators_1.mapTo(true));
        const disconnect$ = this.socketSubject.pipe(operators_1.switchMap((socket) => rxjs_1.fromEvent(socket, 'disconnect')), operators_1.tap(() => this.logger.debug(`Disconnected from socket server [${socketServerAddress}]`)), operators_1.mapTo(false));
        return rxjs_1.merge(connect$, disconnect$).pipe(operators_1.tap(() => {
            const connectedSockets = this.client.io.connecting || [];
            const ids = connectedSockets.filter((s) => s.connected)
                .filter((s) => s.id)
                .map((s) => s.id);
            if (ids.length) {
                this.logger.debug(`Connected socket clients ids: `, ids);
            }
        }));
    }
}
exports.SocketClient = SocketClient;
