/**
 * The BuilderWithoutNulls decorator works the same way as Builder but when any of setters set null to property than that property is deleted.
 * This is needed when document is saved to the mongodb. Nulls will not be saved to database.
 */
export declare function BuilderWithoutNulls<T extends new (...args: any[]) => {}>(constructor: T): {
    new (...args: any[]): {
        newInstance: T;
        toBuilder(): import(".").BuilderType<T>;
        build(): T;
    };
} & T;
