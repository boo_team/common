export declare function Builder<T extends new (...args: any[]) => {}>(constructor: T): {
    new (...args: any[]): {
        newInstance: T;
        toBuilder(): import(".").BuilderType<T>;
        build(): T;
    };
} & T;
