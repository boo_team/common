import { BuilderType } from './BuilderType';
export declare type ToBuilder<T> = T & {
    toBuilder: () => BuilderType<T>;
};
