export declare type BuilderType<T> = {
    [P in keyof T]: (newValue: T[P]) => BuilderType<T>;
} & {
    build: () => T;
};
