"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getBuilderClass = (constructor, assignSetter) => {
    return class extends constructor {
        constructor(...args) {
            super(...args);
        }
        toBuilder() {
            const oldInstance = {};
            const properties = Object.getOwnPropertyNames(this);
            properties.forEach((key) => {
                oldInstance[key] = this[key];
                assignSetter(key, this);
            });
            this.newInstance = oldInstance;
            return this;
        }
        build() {
            return this.newInstance;
        }
    };
};
