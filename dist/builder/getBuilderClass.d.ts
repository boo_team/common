import { BuilderType } from './interface/BuilderType';
export declare const getBuilderClass: <T extends new (...args: any[]) => {}>(constructor: T, assignSetter: (key: string, that: any) => void) => {
    new (...args: any[]): {
        newInstance: T;
        toBuilder(): BuilderType<T>;
        build(): T;
    };
} & T;
