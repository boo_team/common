"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const getBuilderClass_1 = require("./getBuilderClass");
/**
 * The BuilderWithoutNulls decorator works the same way as Builder but when any of setters set null to property than that property is deleted.
 * This is needed when document is saved to the mongodb. Nulls will not be saved to database.
 */
function BuilderWithoutNulls(constructor) {
    return getBuilderClass_1.getBuilderClass(constructor, assignSetterToKeyIfNullDeleteProp);
}
exports.BuilderWithoutNulls = BuilderWithoutNulls;
const assignSetterToKeyIfNullDeleteProp = (key, that) => {
    that[key] = (prop) => {
        if (prop !== null) {
            that.newInstance[key] = prop;
        }
        else {
            delete that.newInstance[key];
        }
        return that;
    };
};
