"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * The Builder decorator change class to builder.
 * When class is in builder mode (toBuilder() method invoked) than all properties are changed to setters.
 * After invoking build() method class returns to previous state. All setters are changed to new properties.
 */
const getBuilderClass_1 = require("./getBuilderClass");
function Builder(constructor) {
    return getBuilderClass_1.getBuilderClass(constructor, assignSetterToKey);
}
exports.Builder = Builder;
const assignSetterToKey = (key, that) => {
    that[key] = (prop) => {
        that.newInstance[key] = prop;
        return that;
    };
};
