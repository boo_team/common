export interface SearchRequestStatusDto {
    readonly searchId: string;
    readonly timestamp: number;
}
