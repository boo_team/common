export interface ProcessingResultStatusDto {
    readonly searchId: string;
    readonly timestamp: number;
}
