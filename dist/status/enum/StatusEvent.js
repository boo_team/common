"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var StatusEvent;
(function (StatusEvent) {
    StatusEvent["SCRAPING_STATUS"] = "SCRAPING_STATUS";
    StatusEvent["PROCESSING_RESULT_STATUS"] = "PROCESSING_RESULT_STATUS";
    StatusEvent["SEARCH_REQUEST_UPDATE"] = "SEARCH_REQUEST_UPDATE";
    StatusEvent["NEW_SEARCH_REQUEST"] = "NEW_SEARCH_REQUEST";
    StatusEvent["SEARCH_REQUEST_DELETED"] = "SEARCH_REQUEST_DELETED";
})(StatusEvent = exports.StatusEvent || (exports.StatusEvent = {}));
