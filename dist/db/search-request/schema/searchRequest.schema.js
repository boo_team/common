"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const checkDate_schema_1 = require("./checkDate.schema");
const childrenProperties_schema_1 = require("./childrenProperties.schema");
exports.SearchRequestSchemaKey = 'searchRequest';
exports.SearchRequestSchema = (schema) => new schema({
    searchId: String,
    priority: Number,
    updateFrequencyMinutes: Number,
    resultsLimit: Number,
    occupancyStatus: String,
    occupancyUpdatedAt: Date,
    owner: String,
    // Scenario parameters
    city: String,
    checkInDate: checkDate_schema_1.CheckDateSchema(schema),
    checkOutDate: checkDate_schema_1.CheckDateSchema(schema),
    numberOfRooms: Number,
    numberOfAdults: Number,
    numberOfChildren: [childrenProperties_schema_1.ChildrenPropertiesSchema(schema)],
    currency: String,
    language: String,
}, {
    versionKey: false,
    timestamps: {
        createdAt: true,
        updatedAt: true,
    },
});
