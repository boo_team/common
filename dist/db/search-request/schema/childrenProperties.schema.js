"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChildrenPropertiesSchema = (schema) => new schema({
    yearAgeAtCheckOut: Number,
}, {
    _id: false,
    versionKey: false,
});
