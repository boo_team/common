import { Schema } from 'mongoose';
import { ChildrenPropertiesDocument } from '..';
export declare const ChildrenPropertiesSchema: (schema: typeof Schema) => Schema<ChildrenPropertiesDocument>;
