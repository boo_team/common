import { Schema } from 'mongoose';
import { CheckDateDocument } from '..';
export declare const CheckDateSchema: (schema: typeof Schema) => Schema<CheckDateDocument>;
