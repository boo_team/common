import { Schema } from 'mongoose';
import { SearchRequestDocument } from '..';
export declare const SearchRequestSchemaKey: string;
export declare const SearchRequestSchema: (schema: typeof Schema) => Schema<SearchRequestDocument>;
