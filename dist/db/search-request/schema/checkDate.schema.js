"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CheckDateSchema = (schema) => new schema({
    year: Number,
    month: Number,
    day: Number,
}, {
    _id: false,
    versionKey: false,
});
