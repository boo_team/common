import { Schema } from 'mongoose';
import { RawRoomDocument } from '..';
export declare const RawRoomSchema: (schema: typeof Schema) => Schema<RawRoomDocument>;
