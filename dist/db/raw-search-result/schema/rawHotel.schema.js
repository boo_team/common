"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const rawRoomSchema_1 = require("./rawRoomSchema");
exports.RawHotelSchema = (schema) => new schema({
    hotelId: String,
    // following parameters always exist
    name: String,
    price: String,
    tax: String,
    distanceFromCenter: String,
    districtName: String,
    coords: String,
    hotelLink: String,
    // following parameters might not be available
    rate: String,
    secondaryRateType: String,
    secondaryRate: String,
    priceWithoutDiscount: String,
    numberOfReviews: String,
    propertyType: String,
    starRating: String,
    newlyAdded: String,
    bonuses: [String],
    rooms: [rawRoomSchema_1.RawRoomSchema(schema)],
}, {
    _id: false,
    versionKey: false,
});
