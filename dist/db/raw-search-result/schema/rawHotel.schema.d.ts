import { Schema } from 'mongoose';
import { RawHotelDocument } from '..';
export declare const RawHotelSchema: (schema: typeof Schema) => Schema<RawHotelDocument>;
