import { Schema } from 'mongoose';
import { RawSearchResultDocument } from '..';
export declare const RawSearchResultSchemaKey: string;
export declare const RawSearchResultSchema: (schema: typeof Schema) => Schema<RawSearchResultDocument>;
