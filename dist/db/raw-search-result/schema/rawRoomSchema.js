"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RawRoomSchema = (schema) => new schema({
    description: String,
    personCount: String,
    beds: String,
}, {
    _id: false,
    versionKey: false,
});
