"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const rawHotel_schema_1 = require("./rawHotel.schema");
exports.RawSearchResultSchemaKey = 'rawSearchResult';
exports.RawSearchResultSchema = (schema) => new schema({
    searchId: String,
    searchPerformedForPlace: String,
    scrapingTimeSeconds: Number,
    searchProcessTimeSeconds: Number,
    hotelsCount: Number,
    hotels: [rawHotel_schema_1.RawHotelSchema(schema)],
}, {
    versionKey: false,
    timestamps: {
        createdAt: true,
        updatedAt: false,
    },
});
