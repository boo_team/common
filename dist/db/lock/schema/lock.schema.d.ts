import { Schema } from 'mongoose';
import { LockDocument } from '..';
export declare const LockSchemaKey: string;
export declare const LockSchema: (schema: typeof Schema) => Schema<LockDocument>;
