"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LockSchemaKey = 'lock';
exports.LockSchema = (schema) => new schema({}, {
    versionKey: false,
    timestamps: {
        createdAt: true,
    },
});
