"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OwnedRequestSchema = (schema) => new schema({
    searchId: String,
    favorites: [String],
}, {
    _id: false,
    versionKey: false,
});
