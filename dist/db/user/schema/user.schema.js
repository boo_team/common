"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ownedRequest_schema_1 = require("./ownedRequest.schema");
exports.UserSchemaKey = 'user';
exports.UserSchema = (schema) => new schema({
    email: String,
    password: String,
    ownedSearchRequests: [ownedRequest_schema_1.OwnedRequestSchema(schema)],
    roles: [String],
    requestsLimit: Number,
}, {
    versionKey: false,
    timestamps: {
        createdAt: true,
        updatedAt: true,
    },
});
