import { Schema } from 'mongoose';
import { UserDocument } from '..';
export declare const UserSchemaKey: string;
export declare const UserSchema: (schema: typeof Schema) => Schema<UserDocument>;
