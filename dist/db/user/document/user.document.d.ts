import { Document } from 'mongoose';
import { OwnedRequestDocument } from './ownedRequest.document';
import { ObjectId } from 'bson';
import { TimestampsDocument } from '../../interface';
export interface UserDocument extends Document, TimestampsDocument {
    _id: ObjectId;
    email: string;
    password: string;
    ownedSearchRequests: OwnedRequestDocument[];
    roles: string[];
    requestsLimit: number;
}
