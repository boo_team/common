"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class AsyncHelper {
    static async forEach(collection, processPromise) {
        for (const element of collection) {
            await processPromise(element);
        }
    }
}
exports.AsyncHelper = AsyncHelper;
