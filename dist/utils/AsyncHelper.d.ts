export declare class AsyncHelper {
    static forEach<T>(collection: Array<T> | Set<T>, processPromise: (element: T) => void): Promise<void>;
}
