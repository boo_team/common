# Bo Project
Application analyzes price trends and suggests the best offers based on data from the most recognizable portal with hotel reservations.

### Prerequisites:
* docker
### Running the application:

Clone repositories: `search-queue-manager`, `data-collector` and `common`, on you local machine.

Go to `common` repository on your local machine and run docker-compose.yml

```
docker-compose up
```
### Create search request:
When the docker env finishes building, you can create search request by calling:

```
POST http://localhost:3000/api/v1/search-request
{
  "updateFrequencyMinutes": 120,
  "priority": 0,
  "resultsLimit": 200,
  "scenarioType": "GET_HOTELS_LIST",
  "scenario": {
    "city": "Berlin",
    "startDate": {
      "year": 2018,
      "month": 9,
      "day": 21
    },
    "endDate": {
      "year": 2018,
      "month": 9,
      "day": 25
    }
  }
}
```

After sending the request, the search will start and the result will be saved into DB.

## TODO:
* webapp
* data analytics service
